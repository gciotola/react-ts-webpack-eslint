# react-ts-webpack-eslint

- React 16.9
- Emotion (css styles with JavaScript)
- Typescript 3.7
- Webpack
- Eslint (StandardJS + Prettier)
