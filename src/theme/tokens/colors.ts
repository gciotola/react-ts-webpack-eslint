export const colors = {
  primary: '#1E1E2C',
  secondary: '#728DCA',
  alternate: '#CBACA7',
  black: '#1e1e1d',
  gray: '#4a4c49',
  grayLight: '#a5a6a4',
  almostWhite: '#f5f5f5',
  success: '#3C7951',
  warning: '#ffc107',
  danger: '#dc3545',
}
