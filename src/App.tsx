import { GlobalInlcudes } from './theme/GlobalIncludes'
import { Hello } from 'app/components/Hello'
import { HelmetProvider } from 'react-helmet-async'
import React from 'react'

export const App: React.FC = () => {
  return (
    <HelmetProvider>
      <GlobalInlcudes />
      <Hello text='World' />
    </HelmetProvider>
  )
}
